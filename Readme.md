# SaveBook

 SaveBook consiste principalmente em uma rede social colaborativa baseada em campanhas para angariar fundos para instituições sem fins lucrativos. providenciando benefícios aos colaboradores através de apoio de instituições privadas.

## Iniciando

Deverá ser executado script de criação do bando de dados localizado no diretório "Fonte DB".

### Pré-requisitos

*Serviço de página TomCat
*Banco de dados MySQL

### Instalando

Descompactar o arquivo com o código fonte no diretório raiz do servidor Web.

```
unzip savebook-master.zip

tar -zxvf savebook-master.tar.gz

tar -zxvf savebook-master.tar
```

## Desenvolvido com

* [Bootstrap](http://getbootstrap.com/) - Framework HTML, CSS e JavaScript
* [MySQL](http://www.mysql.com) - Banco de dados
* [Maven](https://maven.apache.org/) - Gerenciador de Dependências
* [Spring-boot](https://projects.spring.io/spring-boot/) - Framework JavaWeb

## Versionamento

[Controle de versão](https://gitlab.com/jonesradtke/savebook/tags). 

## Autores

* Jones Bunilha Radtke

## Licença 

Este projeto é licenciado com base na GNU.

## LiveDemo

https://powerful-falls-48675.herokuapp.com/
