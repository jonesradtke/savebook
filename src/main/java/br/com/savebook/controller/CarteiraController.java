package br.com.savebook.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.savebook.model.Carteira;
import br.com.savebook.model.Colaborador;
import br.com.savebook.repository.Carteiras;
import br.com.savebook.repository.Colaboradores;

@Controller
@RequestMapping("/carteira")
public class CarteiraController {
	
	@Autowired
	private Carteiras carteiras;
	
	// bonus inicial
	private double bonus = 100;
	
	// Cria carteria
	public Carteira CriarCarteira() {
		Carteira carteira = new Carteira();
		carteira.setSaldo(bonus);
		carteiras.save(carteira);
		return carteira;
	}
	
	public Carteira encontrarCarteira(int carteiraId) {
		Carteira carteira = carteiras.findOne(carteiraId);
		carteiras.save(carteira);
		return carteira;
	}
	
	
	
}


