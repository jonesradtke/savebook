package br.com.savebook.controller;

import java.awt.PageAttributes.MediaType;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.savebook.model.Campanha;
import br.com.savebook.model.Colaborador;
import br.com.savebook.model.Instituicao;
import br.com.savebook.repository.Campanhas;

import br.com.savebook.repository.Carteiras;
import br.com.savebook.repository.Colaboradores;
import br.com.savebook.repository.Instituicoes;

@Controller
@RequestMapping("/")
public class ColaboradorController {
	
	//view utilizadas
	private static final String INDEX  = "./login/index";
	private static final String HOME = "./home/home";
	
	@Autowired
	private Campanhas campanhas;
	
	@Autowired
	private Colaboradores colaboradores;
	
	@Autowired
	private Carteiras carteiras;
	
	@Autowired
	private Instituicoes instituicoes;
	
	@Autowired
	private CarteiraController carteiraC;
	
	// variavel de seção
	private Colaborador colAutentico;

	@RequestMapping()
	public ModelAndView pagianIndex() {
		ModelAndView mv = new ModelAndView(INDEX);
		colAutentico = null;
		mv.addObject("colaborador", new Colaborador());
		return mv;
	}
	
	// cadastra usuario no sistema
	@RequestMapping(method = RequestMethod.POST)
	public String salvar(@Validated Colaborador col, Errors errors, RedirectAttributes attributes) {
		if(errors.hasErrors()) {
			return INDEX;
		}
		try {
			colaboradores.save(col);
			// trata erro sql(caso salve email ja cadastrardo em outro usuario)
		} catch (DataIntegrityViolationException e) {
			attributes.addFlashAttribute("mensagem", "Esse email já esta cadastrdo");
			return "redirect:/";
		}
		attributes.addFlashAttribute("mensagem", "colaborador salvo com Sucesso!");
		return "redirect:/";
	}
	
	// requisita pagina de atualização de cadastro
	@RequestMapping("{id}")
	public ModelAndView paginaAtulizar(@PathVariable ("id") Colaborador col) {
		ModelAndView mv = new ModelAndView("./home/formCad");
		mv.addObject("colaborador", colAutentico);
		mv.addObject(col);
		return mv;
	}
	
	// realiza a atualização cadastral
	@RequestMapping(value = "/cadastrado", method = RequestMethod.POST)
	public ModelAndView atualizaCadastro(@Validated Colaborador col) {
		ModelAndView mv = new ModelAndView(HOME);
		try {
			// cria a carteira para o usuario que cadastrar o cpf
			if (colAutentico.getIdCarteira()==null && col.getCpf()!="") {
				col.setIdCarteira(carteiraC.CriarCarteira());
				colAutentico = col;
				mv.addObject("colaborador", colAutentico);
				mv.addObject("mensagem", "Agora você já tem sua carteira com um bonus de R$ 100,00");
				List<Campanha> todasCamp = campanhas.findAll();
				mv.addObject("campanhas", todasCamp);
				colaboradores.save(col);
				return mv;
			} else {
				// atribui o a carteira ja cadastrada na atualização do cadastro
				if (colAutentico.getIdCarteira() != null) {
					col.setIdCarteira(carteiraC.encontrarCarteira(colAutentico.getIdCarteira().getIdCarteira()));
				}
				mv.addObject("colaborador", colAutentico);
				mv.addObject("mensagem", "Cadastro Atualizado");
				List<Campanha> todasCamp = campanhas.findAll();
				mv.addObject("campanhas", todasCamp);
				colaboradores.save(col);
				return mv;
			}
			// trata erro sql(caso salve email ja cadastrardo em outro usuario)
			} catch (DataIntegrityViolationException e) {
				mv.addObject("colaborador", colAutentico);
				List<Campanha> todasCamp = campanhas.findAll();
				mv.addObject("campanhas", todasCamp);
				mv.addObject("mensagem", "Email já cadastrado para outro colaborador");
				return mv;
			}
	}
	
	// loga usuario no sistema
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView logar( Colaborador col) {
		
		ModelAndView mv = new ModelAndView(INDEX);
		
		// busca usuario pelo email
		List <Colaborador> encontrado = colaboradores.findByEmailContaining(col.getEmail());
		
		// retorna caso não encontre usuario cadastrado
		if (encontrado.isEmpty()) {
			mv.addObject("mensagem", "Usuario Inexistente!");
			return mv;
		}
		
		colAutentico = encontrado.get(0);
		
		//testa senha digitada com a registrarda no banco		
		if (colAutentico.getSenha().equals(col.getSenha())) {
			mv.setViewName(HOME);
			List<Campanha> todasCamp = campanhas.findAll();
			mv.addObject("campanhas", todasCamp);
			mv.addObject("colaborador", colAutentico);
			
			return mv;
		} else {
			mv.addObject("mensagem", "Senha Incorreta!");
			return mv;
		}
	}
	
	@RequestMapping(value="/home{nome}", method = RequestMethod.GET)
	public ModelAndView pesquisar(@RequestParam(value="nome") String nome) {
		List<Colaborador> todosColaboradores = colaboradores.findByNomeContaining(nome);
		ModelAndView mv = new ModelAndView(HOME);
		mv.addObject("colaborador", colAutentico);
		mv.addObject("colaboradores", todosColaboradores);
		return mv;
	}
	
	// retorna para a pagina home (botão Home)
	@RequestMapping("/logado")
	public ModelAndView home(Colaborador col) {
		ModelAndView mv = new ModelAndView(HOME);
		List<Campanha> todasCamp = campanhas.findAll();
		mv.addObject("colaborador", colAutentico);
		mv.addObject("campanhas", todasCamp);
//		mv.addObject("campanhas", "ok");
		return mv;
	}
	
	// lista instituições (botão Instituições)
	@RequestMapping("/instituicoes")
	public ModelAndView instituicoes(Colaborador col) {
		ModelAndView mv = new ModelAndView(HOME);
		List<Instituicao> todasInst = instituicoes.findAll();

		mv.addObject("colaborador", colAutentico);
		mv.addObject("instituicoes", todasInst);
		return mv;
	}
	
	public String qtdCampanhas(Instituicao inst){
		int cont;
		List<Campanha> encontrado = campanhas.findByIdInstituicaoContaining(inst.getIdInstituicao());
		cont = encontrado.size();
		System.out.println(cont);
		return Integer.toString(cont);
	}
	
	//efetua deposito na carteira do colaborador
	@RequestMapping(value="/depositar{saldo}", method = RequestMethod.GET)
	public ModelAndView depositar(@RequestParam(value="saldo") Double saldo) {
		
		System.out.println(colAutentico.getIdCarteira().getSaldo()+saldo);

		ModelAndView mv = new ModelAndView(HOME);
		mv.addObject("colaborador", colAutentico);
				
		colAutentico.getIdCarteira().setSaldo(colAutentico.getIdCarteira().getSaldo()+saldo);
		
		carteiras.save(colAutentico.getIdCarteira());
		
		return mv;
	}
	

	//efetua colaboração e debita da carteira do colaborador
		@RequestMapping(value="/colaborar", method = RequestMethod.GET)
		public ModelAndView colaborar(@RequestParam(value="id") String id, @RequestParam(value="valor") Double valor) {

			ModelAndView mv = new ModelAndView(HOME);
			mv.addObject("colaborador", colAutentico);
			
			if ((colAutentico.getIdCarteira().getSaldo()-valor) >= 0) {
				Campanha camp = campanhas.findOne(Integer.parseInt(id));
				camp.setCredito(camp.getCredito()+valor);
				colAutentico.getIdCarteira().setSaldo(colAutentico.getIdCarteira().getSaldo()-valor);
				carteiras.save(colAutentico.getIdCarteira());
				campanhas.save(camp);
				mv.addObject("mensagem", "Obrigado! por colaborar R$ "+valor+" com a "+camp.getNome());
				return mv;
				}
				mv.addObject("mensagem", "Desculpe! não foi possivel colaborar, saldo insuficiente.");
				return mv;
		}

	// lista camapnha (botão Campanhas)
	@RequestMapping("/campanhas")
	public ModelAndView campanhas() {
		ModelAndView mv = new ModelAndView(HOME);
		List<Campanha> todasCamp = campanhas.findAll();
		mv.addObject("campanhas", todasCamp);
		mv.addObject("colaborador", colAutentico);
		return mv;
	}
}
