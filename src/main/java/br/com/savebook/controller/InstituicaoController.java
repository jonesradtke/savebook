package br.com.savebook.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.savebook.model.Campanha;
import br.com.savebook.model.Instituicao;
import br.com.savebook.repository.Campanhas;
import br.com.savebook.repository.Instituicoes;

@Controller
@RequestMapping("/instituicao")
public class InstituicaoController {
	
	@Autowired
	private Instituicoes instituicoes;
	
	@Autowired
	private Campanhas campanhas;
	
	@RequestMapping("/listaCol")
	public ModelAndView listCol() {
		List<Instituicao> todasInst = instituicoes.findAll();
		ModelAndView mv = new ModelAndView();
		mv.addObject("instituicoes", todasInst);
		return mv;
	}
	
	public String salvar(Instituicao inst) {
		instituicoes.save(inst);
		return "";
	}
	
	
}
