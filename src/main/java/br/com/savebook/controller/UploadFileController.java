package br.com.savebook.controller;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.savebook.model.UploadFile;
import br.com.savebook.repository.UploadFiles;

public class UploadFileController {

	private static final String INDEX = "./login/index";

	@Autowired
	private UploadFiles files;

	@RequestMapping()
	public ModelAndView novo() {
		ModelAndView mv = new ModelAndView("INDEX");
		mv.addObject("file", new UploadFile());
		return mv;
	}

	@RequestMapping(value = "/save_file", method = RequestMethod.POST)
	public @ResponseBody String salvar(MultipartHttpServletRequest request, HttpServletResponse response) {

		UploadFile ufile = new UploadFile();
		
		System.out.println("Ok");

		// 0. notice, we have used MultipartHttpServletRequest

		// 1. get the files from the request object
		Iterator<String> itr = request.getFileNames();

		MultipartFile mpf = request.getFile(itr.next());
		System.out.println(mpf.getOriginalFilename() + " Salvo!");

		try {
			// just temporary save file info into ufile
			ufile.length = mpf.getBytes().length;
			ufile.bytes = mpf.getBytes();
			ufile.type = mpf.getContentType();
			ufile.name = mpf.getOriginalFilename();
			
			files.save(ufile);		

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "ok!";
	}

}
