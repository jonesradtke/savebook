package br.com.savebook.model;

import javax.persistence.*;

import org.springframework.format.annotation.NumberFormat;


@Entity
@Table(name = "CARTEIRA")
public class Carteira {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idCarteira;
	
	@NumberFormat(pattern = "#,##0.00")
	private Double saldo;
	
	public int getIdCarteira() {
		return idCarteira;
	}

	public void setIdCarteira(int idCarteira) {
		this.idCarteira = idCarteira;
	}

	public Double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idCarteira;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Carteira other = (Carteira) obj;
		if (idCarteira != other.idCarteira)
			return false;
		return true;
	}
	
	

}
