package br.com.savebook.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "COLABORADOR")
public class Colaborador {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(unique=true)
	private String email;

	// private Endereco endereco;
	// private Fones fones; 

	@NotEmpty(message = "Nome é obrigatorio")
	private String nome;

	@NotEmpty(message = "Todo usuario deve possuir senha!")
	@Size(min = 5, message = "A senha deve ter no minimo 6 caracteres")
	private String senha;

	private String cpf;
	private char genero;
	private String foneCel;
	private String foneRes;
	private String foneCom;
	private String foneRec;
	private String pais;
	private String estado;
	private String cidade;
	private int cep;
	private char status;

	@OneToOne
//	@MapsId
	@JoinColumn(name="idCarteira", unique=true)
	private Carteira idCarteira;
	
	@OneToOne  
//	@MapsId
	@JoinColumn(name="id_uploadfile", unique=true)
	private UploadFile upload;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public Carteira getIdCarteira() {
		return idCarteira;
	}

	public void setIdCarteira(Carteira idCarteira) {
		this.idCarteira = idCarteira;
	}

	public char getGenero() {
		return genero;
	}

	public void setGenero(char genero) {
		this.genero = genero;
	}

	public String getFoneCel() {
		return foneCel;
	}

	public void setFoneCel(String foneCel) {
		this.foneCel = foneCel;
	}

	public String getFoneRes() {
		return foneRes;
	}
	
	public void setFoneRes(String foneRes) {
		this.foneRes = foneRes;
	}

	public String getFoneCom() {
		return foneCom;
	}

	public void setFoneCom(String foneCom) {
		this.foneCom = foneCom;
	}

	public String getFoneRec() {
		return foneRec;
	}

	public void setFoneRec(String foneRec) {
		this.foneRec = foneRec;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public int getCep() {
		return cep;
	}

	public void setCep(int cep) {
		this.cep = cep;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Colaborador other = (Colaborador) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
