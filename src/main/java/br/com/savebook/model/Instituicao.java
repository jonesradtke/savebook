package br.com.savebook.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Instituicao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idInstituicao;

	@NotEmpty(message = "CNPJ é obrigatorio")
	@NotNull
	private int cnpj;

	@NotEmpty(message = "Razão Social é obrigatório")
	private String razaoSocial;

	private String nomeFantasia;
		public int getIdinstituicao(){
		return idInstituicao;
	}

	@OneToMany
	@JoinColumn(name="idCampanha")
	private List<Campanha> idCampanha;
	
	public int getIdInstituicao() {
		return idInstituicao;
	}
	public void setIdInstituicao(int idInstituicao) {
		this.idInstituicao = idInstituicao;
	}
	public List<Campanha> getIdCampanha() {
		return idCampanha;
	}
	public void setIdCampanha(List<Campanha> idCampanha) {
		this.idCampanha = idCampanha;
	}

	public int getCnpj() {
		return cnpj;
	}

	public void setCnpj(int cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String rasaoSocial) {
		this.razaoSocial = rasaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idInstituicao;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Instituicao other = (Instituicao) obj;
		if (idInstituicao != other.idInstituicao)
			return false;
		return true;
	}

}
