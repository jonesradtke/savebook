package br.com.savebook.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "UPLOADFILE")
public class UploadFile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; 
	
	public int length;
	public byte[] bytes;
	public String name;
	public String type;
	
	public UploadFile() {

	}

	public UploadFile(int length, byte bytes[], String name, String type) {
		this.length = length;
		this.bytes = bytes;
		this.name = name;
		this.type = type;
	}

	public int getIdUploadFile() {
		return id;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public int getLength() {
		return length;
	}

	public String getName() {
		return name; 
	}

	public String getType() {
		return type;
	}

	public void setIdUploadFile(int idUploadFile) {
		this.id = idUploadFile;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UploadFile other = (UploadFile) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	

}
