package br.com.savebook.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.savebook.model.Campanha;

public interface Campanhas extends JpaRepository<Campanha, Integer> {
	public List<Campanha> findByIdInstituicaoContaining(int idInstituicao);

}
