package br.com.savebook.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.savebook.model.Carteira;

public interface Carteiras extends JpaRepository<Carteira, Integer> {
	public List<Carteira> findByIdCarteiraContaining(int idCarteira);

}
