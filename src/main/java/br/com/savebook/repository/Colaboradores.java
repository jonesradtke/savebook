package br.com.savebook.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.savebook.model.Colaborador;

public interface Colaboradores extends JpaRepository<Colaborador, Integer> {
	
	public List<Colaborador> findByIdContaining(int id);
	public List<Colaborador> findByEmailContaining(String email);
	public List<Colaborador> findByNomeContaining(String nome);

}
