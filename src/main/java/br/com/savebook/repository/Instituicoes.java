package br.com.savebook.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.savebook.model.Instituicao;

public interface Instituicoes extends JpaRepository<Instituicao, Integer> {
	
	
}
