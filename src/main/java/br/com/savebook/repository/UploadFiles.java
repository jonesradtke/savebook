package br.com.savebook.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.savebook.model.UploadFile;

public interface UploadFiles extends JpaRepository<UploadFile, Integer> {
	
	public List<UploadFile> findByIdContaining(UploadFile uploadFile);
	public List<UploadFile> findByNameContaining(String name);

}
